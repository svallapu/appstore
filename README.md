This is a Loraine Lab fork of the [Cytoscape App Store code](https://github.com/cytoscape/appstore).

Major differences are:

* The Cytoscape App code branch `wip` (work-in-progress) branch is our `master` branch
* The Cytoscape App code branch `master` branch is our `master-old` branch
* We are modfiying the code for use as an IGB App Store.

## Set-up

Follow instructions in [this Google doc](https://docs.google.com/document/d/1_9C03q6TD5wjLqfVLKsuDsIEQ4-qrS0JhONO5VBOwaA/edit?usp=sharing).
